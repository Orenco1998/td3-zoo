public class Employee {
    private String name;
    private Sex Sex;
    private int age;

    public Employee(String name, Sex sex, int age) {
		this.name = name;
		Sex = sex;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", Sex=" + Sex + ", age=" + age + "]";
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return Sex;
    }

    public void setSex(Sex sex) {
        Sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void examineEnclosure(Enclosure enclosure){
        System.out.println("Information sur l'enclos: \n");
        System.out.println("Nom de l'enclos : " + enclosure.getName() + "\n");
        System.out.println("Superficie de l'enclos : " + enclosure.getSize() + "\n");
        System.out.println("Nombre maximum d'animaux que l'enclos peut contenir : " + enclosure.getNumberMax() + "\n");
        System.out.println("Nombre d'animaux présent dans l'enclos : " + enclosure.getNumberOfAnimal() + "\n");
        System.out.println("Degré de proreté dans l'enclos : " + enclosure.getStateEnclosure() + "\n");
        System.out.println("Liste des animaux présent dans l'enclos : " + enclosure.getListAnimal() + "\n");
    }

    public void cleanEnclosure(Enclosure enclosure){
        enclosure.clean();
    }

    public void feedAnimalEnclosure(Enclosure enclosure){
        enclosure.feadAnimal();
    }

    public void transfertAnimal(Enclosure enclosure, Enclosure enclosure2, Animal animal){
        int preTransfert = enclosure2.getNumberOfAnimal();
        enclosure2.addAnimal(animal);     
        int postTransfert = enclosure2.getNumberOfAnimal();
        if(preTransfert < postTransfert){
            enclosure.removeAnimal(animal);
            System.out.println("Transfert réussis");
        }
        else{
            System.out.println("Transfert impossible, veuillez entrer 2 animaux du même type");
        }
    }
}
