import java.util.ArrayList;
import java.util.Random;

public class NoMammal extends Animal{

    private int incubationTime;

    public NoMammal(String name, Sex sex, double weight, double size, int age, int hunger, boolean sleep, int health, int incubationTime) {
        super(name, sex, weight, size, age, hunger, sleep, health);
        this.incubationTime = incubationTime;
    }

    public int getIncubationTime() {
        return incubationTime;
    }

    public void setIncubationTime(int incubationTime) {
        this.incubationTime = incubationTime;
    }

    public NoMammal layEggs(){
        ArrayList<Sex> sex = new ArrayList<Sex>();
        sex.add(Sex.F);
        sex.add(Sex.M);
        Random rand = new Random();
        return new NoMammal(this.name,sex.get(rand.nextInt(sex.size())),1,1,1,100,false,100, 0);
    }
}
