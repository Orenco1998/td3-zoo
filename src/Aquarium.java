
import java.util.ArrayList;

public class Aquarium extends Enclosure {
	
	private double depth;
	private double salinity;
	
	public Aquarium(String name, int size, int numberMax, ArrayList<Animal> listAnimal, double stateEnclosure,
			double depth, double salinity) {
		super(name, size, numberMax, listAnimal, stateEnclosure);
		this.depth = depth;
		this.salinity = salinity;
	}
	
	public Aquarium(String name, int size, int numberMax, double stateEnclosure,
			double depth, double salinity) {
		super(name, size, numberMax, stateEnclosure);
		this.depth = depth;
		this.salinity = salinity;
	}

	public double getDepth() {
		return depth;
	}

	public void setDepth(double depth) {
		this.depth = depth;
	}

	public double getSalinity() {
		return salinity;
	}

	public void setSalinity(double salinity) {
		this.salinity = salinity;
	}
	
	public String checkSalinity() {
		return "The salinity is at " + this.getSalinity() + "%.";
	}
	
	public String checkDepth() {
		if(this.getDepth() < 10) {
		this.setDepth(10 - this.getDepth());
		return "De l'eau a �t� ajout� � l'auquarium !";
		}
		else {
			return "La quantit� d'eau est d�ja au maximum !";
		}
		
	}
	
}
