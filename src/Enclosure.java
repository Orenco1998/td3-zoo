import java.util.ArrayList;

public class Enclosure {
	
	private final String name;
	private int size;
	private int numberMax;
	private ArrayList<Animal> listAnimal;
	private double stateEnclosure;
	
	public Enclosure(String name, int size, int numberMax, ArrayList<Animal> listAnimal, double stateEnclosure) {
		this.name = name;
		this.size = size;
		this.numberMax = numberMax;
		this.listAnimal = listAnimal;
		this.stateEnclosure = stateEnclosure;		
	}

	public Enclosure(String name, int size, int numberMax, double stateEnclosure) {
		this.name = name;
		this.size = size;
		this.numberMax = numberMax;
		this.listAnimal = new ArrayList<Animal>();
		this.stateEnclosure = stateEnclosure;		
	}
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getNumberMax() {
		return numberMax;
	}

	public void setNumberMax(int numberMax) {
		this.numberMax = numberMax;
	}

	public ArrayList<Animal> getListAnimal() {
		return listAnimal;
	}

	public void setListAnimal(ArrayList<Animal> listAnimal) {
		this.listAnimal = listAnimal;
	}

	public double getStateEnclosure() {
		return stateEnclosure;
	}
	
	public String getStateEnclosureToString() {
		
		if(getStateEnclosure() > 80) {
			return "l'Enclo est propre !";
		}
		if(getStateEnclosure() < 80 && getStateEnclosure() < 40 ) {
			return "l'Enclo est l�g�rement sale !";
		}
		if(getStateEnclosure() < 40) {
			return "l'Enclo est sale !";
		}
		if(getStateEnclosure() == 0) {
			return "l'Enclo est tr�s sale !";
		}
		else return "Erreur d'�tat";
	}

	public void setStateEnclosure(double stateEnclosure) {
		this.stateEnclosure = stateEnclosure;
	}

	public String getName() {
		return name;
	}
	
	public int getNumberOfAnimal() {
		return this.getListAnimal().size();
	}
	
	public void addAnimal(Animal animal) {
		this.getListAnimal().add(animal);
	}
	
	public Animal getAnimalByInt(int i) {
		return listAnimal.get(i);
	}
	
	public void removeAnimal(Animal animal) {
	
		this.getListAnimal().remove(animal);
		
	}
	
	public void feadAnimal() {
		for(Animal a: this.getListAnimal()) {
			a.eat();
		}
	}
	public void healAnimal() {
		for(Animal a: this.getListAnimal()) {
			a.heal();
		}
	}
	public String clean() {
		if(this.getListAnimal().size() == 0 && this.getStateEnclosure() < 50.0) {
			this.setStateEnclosure(100.0);
			return "C'est d�sormais propre !";
		}
		else {
			return "Cette enclos ne peux pas �tre nettoyer car il n'est pas vide ou pas assez salle";
		}
	}	

	@Override
	public String toString() {
		return "Enclosure [name=" + name + ", size=" + size + ", numberMax=" + numberMax + ", listAnimal=" + listAnimal
				+ ", stateEnclosure=" + stateEnclosure + "]";
	}
	
	public void setSalinity(double salinity) {
	}

	public String checkSalinity() {
		return "";
	}
	
	public String checkDepth() {
		return "";
	}

	public double getSalinity() {
		return 0;
	}
	
	
	
}
