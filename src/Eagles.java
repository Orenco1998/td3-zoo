public class Eagles extends NoMammal implements AirAnimal {

    public Eagles(String name, Sex sex, double weight, double size, int age, int hunger, boolean sleep, int health, int incubationTime) {
        super(name, sex, weight, size, age, hunger, sleep, health, incubationTime);
    }

    @Override
    public void talk() {
        System.out.println("Je glatis");
    }

    @Override
    public void fly() {
        System.out.println("Je vole");
    }
}
