import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Zoo{
	
	private String nom;
	private Employee employee;
	private int nbrMaxEnclosure;
	private ArrayList<Enclosure> listEnclosure;
	
	public Zoo(String nom, Employee employee, int nbrMaxEnclosure, ArrayList<Enclosure> listEnclosure) {
		super();
		this.nom = nom;
		this.employee = employee;
		this.nbrMaxEnclosure = nbrMaxEnclosure;
		this.listEnclosure = listEnclosure;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployer(Employee employee) {
		this.employee = employee;
	}

	public int getNbrMaxEnclosure() {
		return nbrMaxEnclosure;
	}

	public void setNbrMaxEnclosure(int nbrMaxEnclosure) {
		this.nbrMaxEnclosure = nbrMaxEnclosure;
	}

	public ArrayList<Enclosure> getListEnclosure() {
		return listEnclosure;
	}

	public void setListEnclosure(ArrayList<Enclosure> listEnclosure) {
		this.listEnclosure = listEnclosure;
	}
	
	public void addEnclosure(Enclosure enclos) {
		this.getListEnclosure().add(enclos);
	}
	
	
	public void addEnclosureIHM() {
		System.out.println("---------------------------------------------------------");
		Scanner scEnclo = new Scanner(System.in);
		System.out.print("Saisir le nombre de place max : ");
		int placeAttr = Integer.parseInt(scEnclo.nextLine());
		System.out.print("Saisir le taille en M� : ");
		int sizeAttr = Integer.parseInt(scEnclo.nextLine());
		String test;
		int action;
		do {
			System.out.print("Voulez vous ajoutez des animaux ? : (oui/non) ");
			test = scEnclo.nextLine();
		} while (!test.equals("oui") && !test.equals("non") ); 
			
			do{
				System.out.println("Quel animal l'enclos pourra t'il recevoir ? : ");
					typeEnclos();

				 action = Integer.parseInt(scEnclo.nextLine());
				 if(action < 1 || action > 8){
				 	System.out.println("Veuillez entrer un chiffre parmis ceux proposé svp !");
				 }
			}while(action < 1 || action > 8);

		int nbr = 0;
		if (test.equals("oui")) {
			do {
				System.out.println("Combien en voulez ?");
				nbr = Integer.parseInt(scEnclo.nextLine());
			} while (nbr > placeAttr );
		}
		switch(action) {
			  case 1:
			  	if(test.equals("oui")){
					this.getListEnclosure().add(new Enclosure("Ours", sizeAttr,placeAttr, createListMammal("Ours",nbr) , 90));
				}else{
					this.getListEnclosure().add(new Enclosure("Ours", sizeAttr,placeAttr , 90));
				}
			    break;
			  case 2:
			  	if(test.equals("oui")){
					this.getListEnclosure().add(new Enclosure("Tigre", sizeAttr,placeAttr, createListMammal("Tigre",nbr) , 90));
				}else{
					this.getListEnclosure().add(new Enclosure("Tigre", sizeAttr,placeAttr , 90));
				}
			    break;
			  case 3:
				  if(test.equals("oui")) {
					  this.getListEnclosure().add(new Enclosure("Faucon", sizeAttr, placeAttr, createListNoMammal("Faucon", nbr), 90));
				  }else{
					  this.getListEnclosure().add(new Enclosure("Faucon", sizeAttr,placeAttr , 90));
				  }
			    break;
			  case 4:
			  	if (test.equals("oui")){
				  this.getListEnclosure().add(new Enclosure("Pingouin", sizeAttr,placeAttr, createListNoMammal("Pingouin",nbr) , 90));
				}else{
					this.getListEnclosure().add(new Enclosure("Pingouin", sizeAttr,placeAttr , 90));
				}
				break;
			  case 5:
			  	if (test.equals("oui")){
					this.getListEnclosure().add(new Enclosure("Loup", sizeAttr,placeAttr, createListMammal("Loup",nbr) , 90));
				}else{
					this.getListEnclosure().add(new Enclosure("Loup", sizeAttr,placeAttr , 90));
				}
				break;
			  case 6:
			  	if(test.equals("oui")){
					this.getListEnclosure().add(new Enclosure("Requin", sizeAttr,placeAttr, createListNoMammal("Requin",nbr) , 90));
				}else{
					this.getListEnclosure().add(new Enclosure("Requin", sizeAttr,placeAttr , 90));
				}
				break;
			  case 7:
			  	if(test.equals("oui")){
					this.getListEnclosure().add(new Enclosure("Poisson rouge", sizeAttr,placeAttr, createListNoMammal("Poisson rouge",nbr) , 90));
				}else {
					this.getListEnclosure().add(new Enclosure("Poisson rouge", sizeAttr,placeAttr , 90));
				}
				break;
			  case 8:
			  	if(test.equals("oui")){
					this.getListEnclosure().add(new Enclosure("Baleine", sizeAttr,placeAttr, createListMammal("Baleine",nbr) , 90));
				}else{
					this.getListEnclosure().add(new Enclosure("Baleine", sizeAttr,placeAttr , 90));
				}
				break;
			  default:
				  System.out.println("???");	  
			}
	}

	public void addAnimalIHM() {
		String sexAnimal;
		int nomAttr;
		System.out.println("---------------------------------------------------------");
		Scanner scAnimal = new Scanner(System.in);
		do {
			System.out.println("Saisir le numero de l'enclos dans lequel vous voulez rajouté votre animal : ");
			for(int i=0;i < this.getListEnclosure().size(); i++)
			{
				System.out.println(" numéro enclos : (" + i + ") : " + this.getListEnclosure().get(i).getName());
			}
			nomAttr = Integer.parseInt(scAnimal.nextLine());
			if (nomAttr < 0 || nomAttr >= this.getListEnclosure().size()){
				System.out.println("Veuillez entrer un chiffre parmis ceux proposé seulement svp !");
			}
		}while(nomAttr < 0 || nomAttr > this.getListEnclosure().size());

		do {
			System.out.println("Quel est le sexe de l'animal? (M/F)");
			sexAnimal = scAnimal.nextLine();
			if (!sexAnimal.equals("M") && !sexAnimal.equals("F")) {
				System.out.println("Veuillez rentrer une des 2 lettre représentant le sexe svp !");
			}
		} while (!sexAnimal.equals("M") && !sexAnimal.equals("F"));
		Sex sex = Sex.valueOf(sexAnimal);

		System.out.print("Saisir le poids de l'animal : ");
		double weightAttr = Double.parseDouble(scAnimal.nextLine());
		System.out.print("Saisir la taille en cm de l'animal : ");
		int sizeAttr = Integer.parseInt(scAnimal.nextLine());
		System.out.print("Saisir l'age de l'animal : ");
		int ageAttr = Integer.parseInt(scAnimal.nextLine());

		ArrayList<String> mamal = new ArrayList<String>(){{
		add("Ours");
		add("Tigre");
		add("Baleine");
		add("Loup");
		}};
		if (mamal.contains(this.getListEnclosure().get(nomAttr).getName())) {
			this.getListEnclosure().get(nomAttr).addAnimal(new Mammal(this.getListEnclosure().get(nomAttr).getName(), sex, weightAttr, sizeAttr, ageAttr, 100, false, 100, 0));
		} else {
			this.getListEnclosure().get(nomAttr).addAnimal(new NoMammal(this.getListEnclosure().get(nomAttr).getName(), sex, weightAttr, sizeAttr, ageAttr, 100, false, 100, 0));
		}

	}	
	public void checkEnclosure(Employee employee) {
		Scanner csc = new Scanner(System.in);
		System.out.println("---------------------------------------------------------");
		System.out.println("Que voulez vous faire ?");
		System.out.println("1 : Nourrir les animaux d'un enclos ----- 2 : Nettoyer un enclos ");
		System.out.println("3 : Transférer un animal ----- 4 : Soigner un animal");
		
		System.out.println("----- 0 : exit -----");
		System.out.println("---------------------------------------------------------");
		String action= csc.nextLine();
		switch(action) {
		  case "1":
			  this.getEnclosureByChoice().feadAnimal();
		    break;
		  case "2":

				Enclosure enclosChoix = this.getEnclosureByChoice();
			  if ( enclosChoix instanceof Aquarium )
			  {
				  System.out.println(enclosChoix.checkDepth());
				  System.out.println(enclosChoix.checkSalinity());
			  }
			  else 
				  {
				  System.out.println(enclosChoix.clean());
				  }
			 
		    break;
		  case "3":
			  	TransfertAnimal(employee);
		    break;
		    
		  case "4":
			  this.getEnclosureByChoice().healAnimal();
			 break;
		  case "5":
			  if (getAnimalByChoice(getEnclosureByChoice()).isSleep())
			  {
				  getAnimalByChoice(getEnclosureByChoice()).wake();
			  }
			  else  {
				  System.out.println( getAnimalByChoice(getEnclosureByChoice()).sleep());
			  }
				  
			  break;
		  default:
			  System.out.println("Option inconnu !");
			  
		}
	}

	private Enclosure getEnclosureByChoice() {
		for(int i=0;i < this.getListEnclosure().size(); i++)
		{
			System.out.println("numéro enclos : (" + i + ") : " + this.getListEnclosure().get(i).getName());
		}
		
		Scanner choicesc = new Scanner(System.in);
		System.out.println("Choisir un enclos : ");
		String choix = choicesc.nextLine();
	
		return this.getListEnclosure().get(Integer.parseInt(choix));
	}
	
	private Animal getAnimalByChoice(Enclosure enclos) {
		for(int i=0;i < enclos.getListAnimal().size(); i++)
		{
			System.out.println("numéro Animal : (" + i + ") : " + enclos.getListAnimal().get(i).getName());
		}
		
		Scanner choicesc = new Scanner(System.in);
		System.out.println("Choisir un animal : ");
		String choix = choicesc.nextLine();
		return enclos.getListAnimal().get(Integer.parseInt(choix));
	}

	
	private ArrayList<Animal> createListMammal(String race, int nbr){
		
		ArrayList<Animal> result = new ArrayList<Animal>();

		for(int i = 0 ;i < nbr; i++) {
			System.out.println("---------------------------------------------------------");
			System.out.println("----------------Création d'animaux---------------------");	
			Scanner scAni = new Scanner(System.in);
			String sexAni;
			do {
				System.out.println("Quel est le sexe? (M/F)");
				sexAni = scAni.nextLine();
			} while (!sexAni.equals("M") && !sexAni.equals("F") ); 
			Sex sex = Sex.valueOf(sexAni);
			System.out.print("Saisir le poids de l'animal : ");
			int poidsAni = Integer.parseInt(scAni.nextLine());
			System.out.print("Saisir la taille de l'animal : ");
			int sizeAni = Integer.parseInt(scAni.nextLine());
			System.out.print("Saisir l'age de l'animal : ");
			int ageAni = Integer.parseInt(scAni.nextLine());
			
			result.add(new Mammal(race, sex, poidsAni, sizeAni,ageAni, 100, false, 100, 0));
			
		}
				
		return result;
	}

	private ArrayList<Animal> createListNoMammal(String race, int nbr){
		
		ArrayList<Animal> result = new ArrayList<Animal>();

		for(int i = 0 ;i < nbr; i++) {
			System.out.println("---------------------------------------------------------");
			System.out.println("----------------Création d'animaux---------------------");	
			Scanner scAni = new Scanner(System.in);
			String sexAni;
			do {
				System.out.println("Quel est le sexe? (M/F)");
				sexAni = scAni.nextLine();
			} while (!sexAni.equals("M") && !sexAni.equals("F") ); 
			Sex sex = Sex.valueOf(sexAni);
			System.out.print("Saisir le poids de l'animal : ");
			int poidsAni = Integer.parseInt(scAni.nextLine());
			System.out.print("Saisir la taille de l'animal : ");
			int sizeAni = Integer.parseInt(scAni.nextLine());
			System.out.print("Saisir l'age de l'animal : ");
			int ageAni = Integer.parseInt(scAni.nextLine());
			
			result.add(new NoMammal(race, sex, poidsAni, sizeAni,ageAni, 100, false, 100, 0));
			
		}
				
		return result;
	}
	
	@Override
	public String toString() {
		return "Zoo [nom=" + nom + ", employer=" + employee + ", nbrMaxEnclosure=" + nbrMaxEnclosure
				+ ", listEnclosure=" + listEnclosure + "]";
	}
	
	public int getNumberAnimal() {
		int result = 0;
		for(int i = 0 ; i < listEnclosure.size(); i++)
		{
			result = result + listEnclosure.get(i).getNumberOfAnimal();
		}
		
		return result;
	}
	
	public String getAllAnimal() {
		String result = "Liste des animaux : ";
		for(int i = 0 ; i < listEnclosure.size() ; i++) {
			result = result + listEnclosure.get(i).toString() + (i+1 == listEnclosure.size()? "" : ",");
		}
		return result;		
	}
	
	public String getAllEnclosure() {
		String result = "Liste des Enclos : ";
		for(int i = 0 ; i < listEnclosure.size() ; i++) {
			result = result + listEnclosure.get(i).toString() + (i+1 == listEnclosure.size()? "" : ",");
		}
		return result;
	}

	
	public void randomizeAnimal() {
		int randomAffect = ThreadLocalRandom.current().nextInt(1, 20 + 1);
		int randomEnclosure = ThreadLocalRandom.current().nextInt(0, listEnclosure.size() );
		int randomAnimal = ThreadLocalRandom.current().nextInt(0, listEnclosure.get(randomEnclosure).getListAnimal().size() );
		listEnclosure.get(randomEnclosure).getAnimalByInt(randomAnimal).setHealth(listEnclosure.get(randomEnclosure).getAnimalByInt(randomAnimal).getHealth()-randomAffect);
		listEnclosure.get(randomEnclosure).getAnimalByInt(randomAnimal).setHunger(listEnclosure.get(randomEnclosure).getAnimalByInt(randomAnimal).getHunger()-randomAffect);
		listEnclosure.get(randomEnclosure).getAnimalByInt(randomAnimal).setSleep(!listEnclosure.get(randomEnclosure).getAnimalByInt(randomAnimal).isSleep());		
	}
	
	public void randomizeEnclosure() {
		int randomAffect = ThreadLocalRandom.current().nextInt(1, 20 + 1);
		int randomEnclosure = ThreadLocalRandom.current().nextInt(0, listEnclosure.size());
		
		listEnclosure.get(randomEnclosure).setStateEnclosure(listEnclosure.get(randomEnclosure).getStateEnclosure() - randomAffect);
		if(listEnclosure.get(randomEnclosure) instanceof Aquarium)
		{
			listEnclosure.get(randomEnclosure).setSalinity(listEnclosure.get(randomEnclosure).getSalinity() - randomAffect);
		}
		

	
	}
		public void TransfertAnimal(Employee employee) {
		
			System.out.println("Choisir l'enclos de l'animal a transférer puis choisir l'animal");
			Enclosure enclosDepart = getEnclosureByChoice();
			Animal animalToMove = this.getAnimalByChoice(enclosDepart);
			System.out.println("Choisir l'enclos de destination");
			ArrayList<Enclosure> filtre= new ArrayList<Enclosure>();
			for(int i=0;i < this.getListEnclosure().size(); i++)
			{
				if(this.getListEnclosure().get(i).getName() == animalToMove.getName()) {
					filtre.add(this.getListEnclosure().get(i));
				}
				//System.out.println(" numéro enclos : (" + i + ") : " + this.getListEnclosure().get(i).getName());
			}			
			for(int y=0;y < filtre.size(); y++)
			{
				System.out.println(" numéro enclos : (" + y + ") : " + filtre.get(y).getName());
			}
			Scanner choix = new Scanner(System.in);
			Enclosure enclosDestination = filtre.get(Integer.parseInt(choix.nextLine()));
			employee.transfertAnimal(enclosDepart, enclosDestination, animalToMove);
			
			
		}
	
		private void typeEnclos() {
		
		System.out.println("1 : Ours");
		System.out.println("2 : Tigre");
		System.out.println("3 : Faucon");
		System.out.println("4 : Pingouin");
		System.out.println("5 :	Baleine");
		System.out.println("6 : Loup");
		System.out.println("7 :	Requin");
		System.out.println("8 :	Poisson rouge");
	}

}
