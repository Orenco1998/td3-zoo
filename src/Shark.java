public class Shark extends NoMammal implements MarineAnimal {
    public Shark(String name, Sex sex, double weight, double size, int age, int hunger, boolean sleep, int health, int incubationTime) {
        super(name, sex, weight, size, age, hunger, sleep, health, incubationTime);
    }

    @Override
    public void talk() {
        System.out.println("...");
    }

    @Override
    public void swim() {
        System.out.println("Je nage");
    }
}
