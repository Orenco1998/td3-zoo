
 abstract public class Animal {

	protected String name;
    protected Sex sex;
    protected double weight;
    protected double size;
    protected int age;
    protected int hunger;
    protected boolean sleep;
    protected int health;

    public Animal(String name, Sex sex, double weight, double size, int age, int hunger, boolean sleep, int health) {
        this.name = name;
        this.sex = sex;
        this.weight = weight;
        this.size = size;
        this.age = age;
        this.hunger = hunger;
        this.sleep = sleep;
        this.health = health;
    }

    public void eat(){
        if(!this.isSleep()){
        	 System.out.println("Cet animal ne peux pas manger, il dort") ;
        }else{
        	this.setHunger(100);
        	 System.out.println("Cet animal à finis de manger");
        }
    }

    public void talk(){

    }

    public void heal(){
    	this.health = 100;
    	System.out.println("Cet animal est soigné");
    }

    public String sleep(){
        this.setSleep(true);
        return"ZZZzzz";
    }

    public String wake(){
        this.setSleep(false);
        return "Cet animal est réveillé";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHunger() {
        return hunger;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public boolean isSleep() {
        return sleep;
    }

    public void setSleep(boolean sleep) {
        this.sleep = sleep;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
    
    @Override
	public String toString() {
		return "Animal [name=" + name + ", sex=" + sex + ", weight=" + weight + ", size=" + size + ", age=" + age
				+ ", hunger=" + hunger + ", sleep=" + sleep + ", health=" + health + "]";
	}
}
