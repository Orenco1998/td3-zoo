public class Whales extends Mammal implements MarineAnimal {

    public Whales(String name, Sex sex, double weight, double size, int age, int hunger, boolean sleep, int health, int gestationTime) {
        super(name, sex, weight, size, age, hunger, sleep, health, gestationTime);
    }

    @Override
    public void talk() {
        System.out.println("Pshhhhh");
    }
    @Override
    public void swim() {
        System.out.println("Je nage");
    }
}
