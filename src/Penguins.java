public class Penguins extends NoMammal implements MarineAnimal,AirAnimal {

    public Penguins(String name, Sex sex, double weight, double size, int age, int hunger, boolean sleep, int health, int incubationTime) {
        super(name, sex, weight, size, age, hunger, sleep, health, incubationTime);
    }

    @Override
    public void talk() {
        System.out.println("Je braie");
    }

    @Override
    public void fly() {
        System.out.println("Je vole");
    }

    @Override
    public void swim() {
        System.out.println("Je nage");
    }
}
