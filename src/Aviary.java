import java.util.ArrayList;

public class Aviary extends Enclosure  {
	
	private double heigth;


	public Aviary(String name, int size, int numberMax, ArrayList<Animal> listAnimal, double stateEnclosure, double heigth) {
		super(name, size, numberMax, listAnimal, stateEnclosure);
		this.heigth = heigth;
	}
	
	public Aviary(String name, int size, int numberMax, double stateEnclosure, double heigth) {
		super(name, size, numberMax, stateEnclosure);
		this.heigth = heigth;
	}

	public double getHeigth() {
		return heigth;
	}

	public void setHeigth(double heigth) {
		this.heigth = heigth;
	}

	@Override
	public String toString() {
		return "Aviary [heigth=" + heigth + ", toString()=" + super.toString() + "]";
	}

	public String clean() {
		if(this.getStateEnclosure() < 50.0 && this.getListAnimal().size() ==0) {
			return super.clean() + "Le toit aussi est propre!";
		}
		else {
			return super.clean();
		}
	}
	
	
	
	

}
