public class Bear extends Mammal implements TerrestrialAnimal {

    public Bear(String name, Sex sex, double weight, double size, int age, int hunger, boolean sleep, int health, int gestationTime) {
        super(name, sex, weight, size, age, hunger, sleep, health, gestationTime);
    }

    @Override
    public void talk() {
        System.out.println("Je grogne");
    }

    @Override
    public void walk() {
        System.out.println("je vagabonde");
    }
}
