import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

    	
		boolean menu = true;
		Scanner sc = new Scanner(System.in);
		System.out.println("Bonjour vous aller commencer une nouvelle aventure dans notre ZOO !");
		System.out.println("Quel est votre nom? ");
		String nomEmployee= sc.nextLine();
		System.out.println("Quel est votre age? ");
		int ageEmployee= Integer.parseInt(sc.nextLine());
		String sexEmployee;
		
		do {
			System.out.println("Quel est votre sexe? (M/F)");
			sexEmployee = sc.nextLine();
		} while (!sexEmployee.equals("M") && !sexEmployee.equals("F") ); 
		Sex sex = Sex.valueOf(sexEmployee);

		Employee employee = new Employee(nomEmployee, sex, ageEmployee);
		System.out.println(employee.toString());
		System.out.println("Dans un premier temps veuillez choisir un nom pour votre zoo : ");
		String nomZoo = sc.nextLine();
		System.out.println( "Voici le nom du Zoo : " + nomZoo);
		ArrayList<Animal> mammal = new ArrayList<Animal>(){{
			add(new Mammal("Tigre",sex, 20,20,12,12,false,90,0));
			}};
		Enclosure EnclosTiger = new Enclosure("Tigre", 50, 10, mammal , 90);
		//Enclosure EnclosBear = new Enclosure("Ours", 50, 10, 40);
		//Aquarium aquariumWhale = new Aquarium("Baleine",50 ,5 ,90 ,10 ,90);
    	//Aviary AviaryEagles = new Aviary("Aigle",20 ,15 ,90 ,20);
    	ArrayList<Enclosure> listEnclos = new ArrayList<Enclosure>();
    	listEnclos.add(EnclosTiger);
    	//listEnclos.add(EnclosBear);
    	//listEnclos.add(aquariumWhale);
    	//listEnclos.add(AviaryEagles);
    	Zoo zoo = new Zoo(nomZoo, employee, 20, listEnclos);
    	System.out.println(zoo.toString());
    	
		/////////////////////Menu de base//////////////////////////////////////////
		
		while(menu) {
			//zoo.randomizeAnimal();
			//zoo.randomizeEnclosure();
			System.out.println("---------------------------------------------------------");
			System.out.println("Que voulez vous faire ?");
			System.out.println("1 : Cr�ation d'un enclos ----- 2 : Voir les Enclos du parc ");
			System.out.println("3 : Cr�ation d'animaux ----- 4 : Voir les animaux du parc");
			System.out.println("5 : S'occupper des enclos");
			System.out.println("----- 0 : exit -----");
			System.out.println("---------------------------------------------------------");
			String action= sc.nextLine();
			switch(action) {
			  case "1":
				  zoo.addEnclosureIHM();
			    break;
			  case "2":
				  System.out.println(zoo.getAllEnclosure());
			    break;
			  case "3":
					zoo.addAnimalIHM();
			    break;
			  case "4":
				  System.out.println(zoo.getAllAnimal());
			    break;
			  case "5":
				  zoo.checkEnclosure(employee);
			    break;			    
			  case "0":
				  menu = false;
				break;
				
			  default:
				  System.out.println("Option inconnu !");
				  
			}
		}
		System.out.println("..Fin de la simulation..");
	
		/////////////////////Menu de base//////////////////////////////////////////
    }
}
