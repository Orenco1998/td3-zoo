public class Wolf extends Mammal implements TerrestrialAnimal {


    public Wolf(String name, Sex sex, double weight, double size, int age, int hunger, boolean sleep, int health, int gestationTime) {
        super(name, sex, weight, size, age, hunger, sleep, health, gestationTime);
    }

    @Override
    public void talk() {
        System.out.println("Ahooouuuu");
    }

    @Override
    public void walk() {
        System.out.println("Je vagabonde");
    }
}
