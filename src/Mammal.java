import java.util.Random;
import java.util.ArrayList;
public class Mammal extends Animal {

    private int gestationTime;

    public Mammal(String name, Sex sex, double weight, double size, int age, int hunger, boolean sleep, int health, int gestationTime) {
        super(name, sex, weight, size, age, hunger, sleep, health);
        this.gestationTime = gestationTime;
    }

    public int getGestationTime() {
        return gestationTime;
    }

    public void setGestationTime(int gestationTime) {
        this.gestationTime = gestationTime;
    }

    public Mammal giveBirth() {
        ArrayList<Sex> sex = new ArrayList<Sex>();
        sex.add(Sex.F);
        sex.add(Sex.M);
        Random rand = new Random();
        return new Mammal(this.name,sex.get(rand.nextInt(sex.size())),1,1,1,100,false,100, 0);
    }
}
